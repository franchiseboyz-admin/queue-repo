from sqlalchemy import Column, Integer, Boolean, DATETIME, String

from datetime import datetime
from app import Base

class Token(Base):
    __tablename__ = 'tokens'

    id = Column(Integer, primary_key = True)
    name = Column(String(10), nullable = False, unique = True)
    served = Column(Boolean, default = False, nullable = False)

    def __repr__(self):
        return f'<Token {self.name!r}>'

class Timimgs(Base):
    __tablename__ = 'timings'
    id = Column(Integer, primary_key = True)
    event = Column(Boolean, nullable = False)
    time = Column(DATETIME, nullable = False, default = datetime.utcnow)

    def __repr__(self):
        return '<Timings %r>' % self.event
    




